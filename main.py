from collections import defaultdict

import requests

from typing import Dict

from abc import abstractmethod, ABC
from xml.etree import ElementTree


class Transformer(ABC):
    @abstractmethod
    def map(self, mapping):
        pass


class DefaultTransformer(Transformer):
    """Маппинг данных из словаря на объекты

    Пример маппинга,
        [{'path': 'rss.channel.item', 'model': Item, 'many': True, 'base': True,
          'attrs': [{'name': 'title', 'path': 'title', 'factory': str},
           {'name': 'pubDate', 'path': 'pubDate', 'factory': lambda x: datetime.strptime(x, "%a, %d %b %Y %H:%M:%S %Z")},
           {'name': 'creator', 'path': 'creator', 'model': Creator,
            'attrs': [{'name': 'name', 'path': 'name'}]},
           {'name': 'category', 'path': 'category', 'model': Category, 'many': True,
            'attrs': [{'name': 'label', 'path': None}]}
           ]
        }]
    """
    def __init__(self, mapping):
        self._mapping = mapping
        self._inner_instances = {}

    def map(self, data):
        ret = []
        for entry in self._mapping:
            if not entry.get('model'):
                continue
            entry_data = self.process_model(entry, self.get_data_by_path(data, entry['path']))
            ret.append(entry_data)
        return ret

    def process_model(self, entry, data):
        ret = None
        if data is not None:
            if entry.get('many', False):
                ret = [self.process_attrs(entry, item_data) for item_data in data]
            else:
                ret = self.process_attrs(entry, data)

        if entry.get('model') and not entry.get('base', False):
            self._inner_instances[entry['name']] = ret
            ret = None

        return ret

    def process_attrs(self, entry, item):
        """Обрабатывает модель, итерируя по ее атрибутам.

        Для "базовой" модели возвращает словарь с "внутренними" инстансами и именами атрибутов,
        к которым они относятся.
        """
        model = self.get_model(entry.get('model'))
        instance = model()
        for attr in entry['attrs']:
            data = self.get_data_by_path(item, attr['path'])
            if attr.get('model'):
                value = self.process_model(attr, data)
            else:
                value = attr.get('factory', str)(data)
            setattr(instance, attr['name'], value)

        ret = instance
        if entry.get('base', False):
            # Если возвращаем "базовую" модель, то обнуляем словарь "внутренних" классов
            ret = (instance, self._inner_instances)
            self._inner_instances = {}
        return ret

    def get_data_by_path(self, data, path):
        """Возвращает данные из словаря по пути вида dot-нотации"""
        if path is None:
            return data

        keys_list = path.split('.')
        try:
            for idx in range(0, len(keys_list)):
                data = data[keys_list[idx]]
        except KeyError:
            data = None
        return data

    def get_model(self, factory_class):
        """Возвращает класс для обработки атбибута"""
        # Подгружаем класс, если фабрику передали строкой
        if isinstance(factory_class, str):
            components = factory_class.split('.')
            mod = __import__(components[0])
            for comp in components[1:]:
                mod = getattr(mod, comp)
            return mod
        return factory_class


class Loader(ABC):
    @abstractmethod
    def load(self):
        pass


class Saver(ABC):
    @abstractmethod
    def save(self, data):
        pass


class PrintSaver(Saver):
    def save(self, data):
        for element in data:
            for entry, linked_items in element:
                print(entry)
                print(linked_items)
                print('='*80)


class DjangoSaver(Saver):
    """Сохраняем, но с дублями."""
    def save(self, data):
        for element in data:
            for entry, linked_items in element:
                many_relation = {}
                for attr_name, one_or_more_items in linked_items.items():
                    if isinstance(one_or_more_items, list):
                        # m2m добавляем в модель после ее сохранения
                        many_relation[attr_name] = [obj.save() for obj in one_or_more_items]
                    else:
                        # fk сохраняет сразу
                        setattr(entry, attr_name, one_or_more_items.save())

                entry.save()
                # Сохраняем m2m
                for attr_name, items in many_relation.values():
                    values = [item.save() for item in items]
                    getattr(entry, attr_name).add(*values)


class RSSLoader(Loader):
    def __init__(self, url):
        self.url = url

    def load(self):
        text = requests.get(self.url).text
        tree = ElementTree.fromstring(text)
        return self.etree2dict(tree)

    def etree2dict(self, tree: ElementTree.Element) -> Dict:
        if '{' in tree.tag and '}' in tree.tag:
            namespace = "{" + tree.tag.partition("{")[2].partition("}")[0] + "}"
        else:
            namespace = ''
        tag_name = tree.tag[len(namespace):]
        tree_dict = {tag_name: {} if tree.attrib else None}
        childrens = list(tree)
        if childrens:
            def_dict = defaultdict(list)
            for dchild in map(self.etree2dict, childrens):
                for key, val in dchild.items():
                    def_dict[key].append(val)
            tree_dict = {tag_name: {key: val[0] if len(val) == 1 else val for key, val in def_dict.items()}}
        if tree.attrib:
            tree_dict[tag_name].update(('@' + key, val) for key, val in tree.attrib.items())
        if tree.text:
            text = tree.text.strip()
            if childrens or tree.attrib:
                if text:
                    tree_dict[tag_name]['#text'] = text
            else:
                tree_dict[tag_name] = text
        return tree_dict


class Importer:
    def __init__(self, loader: Loader, transformer: Transformer, saver: Saver):
        self.loader = loader
        self.transformer = transformer
        self.saver = saver

    def do_import(self):
        data = self.loader.load()
        data = self.transformer.map(data)
        self.saver.save(data)
